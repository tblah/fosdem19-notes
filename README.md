# FOSDEM19 report

## Talks
This repo holds my notes for each talk I attended.

To highlight some in particular:
- `sat/03-kernel-memory-disclosure-detection.md` was a good talk about leaking kernel memory through syscalls. The example given was a freebsd syscall which zeroed only used struct fields and not the struct's padding before copying the struct to userspace.
- `sat/06-gnu-mes.md` was a talk about the Guix SD project's attempt to bootstrap their distribution from as little binary as possible. The core of the problem is that to compile GCC you need a previous version of GCC, which is a massive binary in which all kinds of malware could be hidden. Guix is working to get the initial binary down to only a very simple assembler.
- `sun/03-redoxos.md` this talk was slow on the technical content but interestingly *the speaker works at ARM promoting the use of open source software for safety-critical applications, especially automotive*. In the talk he strongly promoted Rust as a good way of writing safety-critical software, using RedoxOS as an example of complex system software which has been able to make very quick progress as a result of Rust's features as a programming language. I hope the speaker can get this policy adopted by car companies so we can have lots of Rust projects!

## Extra-curricular
I enjoyed walking around Brussels and seeing all of the fancy palaces and the European Parliament. On Monday morning I visited an animated Van Goth exhibition which was projected onto the inside of a big 19th century gallery. Seeing Van Goth's work animated like this was breathtaking.
