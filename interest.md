# Fosdem Notes

## Saturday
### Janson (main)
9:30 - 9:55 Welcome to FOSDEM 2019
10:00 - 10:50 Can Anyone Live in Full Software Freedom Today?
15:00 - 15:50 DNS over HTTPS
16:00 - 16:50 Netflix and FreeBSD
17:00 - 17:50 The TPM2 Software Community

### K.1.105 (La Fontaine)
**12:00 - 12:50 ELI5: ZFS Caching - Alan Jude**
14:00 - 14:50 Square Kilometre Array and its Software Defined Supercomputer

### K.3.401
10:30 - 10:55 25 Years of FreeBSD
12:35 - 12:55 Embedded FreeBSD on RISC-V
13:00 - 13:20 An Update on NetBSD
*13:25 - 13:55 KLEAK*
14:05 - 14:40 FreeBSD Graphics
16:15 - 16:55 OpenBSD as a full-featured NAS
**17:00 - 17:40 ZFS Powered magic Upgrades - Alan Jude**

### UA2.114 (Baudoux)
11:40 - 12:10 A Year of Container Kernel Work

### K.4.201
14:50 - 15:20 GNU Guix
15:20 - 15:50 Reduced Binary sized bootstrap for GuixSD
15:50 - 16:20 Guile 3: faster programs with JIT compilation
16:20 - 16:50 A Guiler's Year of Racket
16:50 - 17:20 Fractalide and Cantor (Racket)
17:20 - 18:20 Makeyour own language with Racket

### AW1.126
10:30 - 11:15 LLVM+Clang for RISC-V
14:15 - 14:45 How compact is compiled RISC-V code?
14:45 - 15:15 FreeRTOS on RISC-V
15:30 - 16:15 Lessons learned from porting HelenOS to RISC-V
16:15 - 17:00 Updates from the RISC-V Trusted Execution Environment Group
17:15 - 18:00 Using SAIL to generate GNU assembler/dissasembler and simulator for RISC-V
18:00 - 18:30 Buildroot for RISC-V

---------------------------------------------------------------
## Sunday
### Janson
11:00 - 11:50 The Current and Future Tor Project
13:00 - 13:50 Open Source at DuckDuckGo
16:00 - 16:50 The Cloud is Just Another Sun
17:00 - 17:50 50 Years of Unix and Linux advances
17:55 - 18:00 Closing FOSDEM

### K.1.105 (La Fontaine)
10:00 - 10:50 Tesla hacking to Freedom EV
11:00 - 11:50 Embedded Go

### K.3.401
16:30 - 17:00 GNU Guix's Take on a new approach to software distribution

### K.4.201
09:00 - 9:40 Roll your own compiler with LLVM
09:45 - 10:25 Rewriting Pointer Dereferences in bcc with Clang
15:45 - 16:25 Compiling the Linux Kernel with LLVM tools
16:30 - 17:00 It was working yesterday! Investigating regressions with llvmlab bisect

### AW1.121
09:55 - 10:30 Microkernel virtualization under one roof
10:40 - 11:00 A roadmap for the Hurd
11:10 - 11:55 RedoxOS
12:05 - 12:50 Hands-on composition of basic L4Re components
13:00 - 13:35 Unikraft: Unikernels made easy
13:45 - 14:20 Hardware/Software Co-Design for Efficient Microkernel Execution
14:30 - 15:15 Solo5: A sandboxed, retargetable execution environment for unikernels
16:05 - 16:30 Operating System hardening: Dealing with external interupts
16:35 - 17:00 The impact of Meltre and Specdown on microkernel systems

### UD2.208 (Decroly)
09:00 - 09:25 RustPython: a Python implementation in Rust
09:30 - 10:15 What is Rust doing behind the curtains?
13:20 - 13:45 Profiling Rust

### UD2.218A
09:30 - 09:55 Open source software security testing
10:30 - 10:55 Russian crypto algorithms in the OpenSource world
11:30 - 11:55 Tracking users with core Internet protocols
12:00 - 12:55 How to prevent cryptographic pitfalls by design
12:30 - 12:55 USB borne attacks and usable defense mechanisms
14:00 - 14:25 CHIPSEC on non-UEFI Platforms
16:00 - 16:25 SpamAssasin 4.0
