# Fosdem plan

## Saturday
9:30 - 9:55 Welcome to FOSDEM 2019 (Janson)
...
10:30 - 11:15 LLVM+Clang for RISC-V (AW1.126)
...
**12:00 - 12:50 ELI5: ZFS Caching - Alan Jude** (K.1.105 La Fontaine)
...
*13:25 - 13:55 KLEAK* (K.3.401)
14:05 - 14:40 FreeBSD Graphics (K.3.401)
...
14:50 - 15:20 Building a Whole Distro on Top of a Minimalistic Language (K.4.201)
15:20 - 15:50 Reduced Binary sized bootstrap for GuixSD (K.4.201)
15:50 - 16:20 Guile 3: faster programs with JIT compilation (K.4.201)
16:20 - 16:50 A Guiler's Year of Racket (K.4.201)
...
**17:00 - 17:40 ZFS Powered magic Upgrades - Alan Jude** (K.3.401)
...

## Sunday
09:45 - 10:25 Rewriting Pointer Dereferences in bcc with Clang (K.4.201)
...
10:40 - 11:00 A roadmap for the Hurd (AW1.121)
11:10 - 11:55 RedoxOS (AW1.121)
12:05 - 12:50 Hands-on composition of basic L4Re components (AW1.121)
13:00 - 13:35 Unikraft: Unikernels made easy (AW1.121)
13:45 - 14:20 Hardware/Software Co-Design for Efficient Microkernel Execution (AW1.121)
14:30 - 15:15 Solo5: A sandboxed, retargetable execution environment for unikernels (AW1.121)
...
16:05 - 16:30 Hardening External Interrupts (AW1.121)
16:36 - 17:00 Meltre and Specdown on Microkernel Systems (AW1.121)
