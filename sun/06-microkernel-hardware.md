# 13:45 - 14:20 Hardware/Software Co-Design for Efficient Microkernel Execution (AW1.121)
- Speaker: Martin Decky

"Microkernel multiserver systems are better than monolithic systems"
    - Biggs S., Lee D., Heiser G.: The Jury Is In: Monolithic OS Design is Flawed: Microkernel-based Designs Improve Security, ACM 9th Asia-Pacific Workshop on Systems (2018)
    - But thre is a performance price for using a mirokernel system
        - IPC between address spaces

## Problem Statement
- Operating Systems always used to be written for CPUs, only after the CPUs were released
- ISAs haven't really changed since IBM System/370
    - No wonder microkernels suffer a performance penalty
- What if we design hardware with an ISA suitable for microkernels?

"The only way to make Linux more secure is to throw it out"

## Communication Between Address Spaces
- In monolithic kernels, subsystems communicate with just a function-call
- In microkernels
    - Privilege level switch, address space switch, scheduling, copying or memory sharing
- Ideas
    - Richer Jump/Call and Return instructions to avoid the kernel round trip
        - CPU changes the address space without the privilage level switch
        - Parties identified by a "call gate"
        - Call gates stored in a TLB-like hardware cache
        - Cache populated by the microkernel similarly to TLB-only memory management
    - Asynchronous IPC:
        - Use CPU cache lines as buffers for the messages
        - Async jump/call, async return and async receive instructions
        - Using the CPU cache line like an extended register stack engine
            - Intel IA-32 Task State Segment

## Bulk data
- Memory sharing is actually quite efficient for large amounts of data
    - Overhead is caused primarily by creating and tearing down the shared pages
    - Data needs to be page-aligned
- Sub-page granularity and dynamic data structures
    - Suggestion: Using CPU cache lines as shared buffers

## Fast Context Switching
- Hardware multi-threading (hyperthreading?)
    - Effective
    - Does not scale beyond a few threads
- Operating system context switching
    - Too slow (10us)
    - Scales for any thread count
- We need a middle-ground between these two
    - Dedicated instructions for context store, context restore,context switch, context save, context load
    - Autonomous mechanism for event-triggered context switch (interrupts)
        - Userspace based interrupt processing
            - So that user-space microkernel servers can handle interrupts
        - We could get rid of polling for extreme low latency

## (Stretch) Capabilities as First-Class Entities (hardware enforced unforgeable object identifiers)
- Embed the capability reference in pointers
- Risc-V 128bit variant could use 64-bits for the pointer and 64-bits for the capability reference
- This could simplify managed languages' VMs
- Working with multiple virtual address spaces at once

## Questions
- What about the Cherry Microarchitecture from Cambridge? Isn't that similar?
    - Yes. They have some of the same ideas but don't go far enough. Also see Intel SCC
- Further back in time: LISP Machines, etc all failed - killed by More's Law
    - We are no longer in More's Law so maybe this will go better now
