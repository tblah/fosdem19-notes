# 16:36 - 17:00 Meltre and Specdown on Microkernel Systems (AW1.121) - Matthias Lange, Kernkonzept
- Talk is about L4Re

## Meltdown
- Classic 32-bit virt address layout:
    - Kernel in 3GB to 4GB memory addresses
    - User in 0 - 3GB memory addresses
- User can cause the CPU to speculatively execute reads from kernel memory
    - Effects seen in L1 Cache
    - Accessible pretty quickly through side-channels
- L4Re uses very big pages for performance reasons
    - So kernel mappings may include user memory
- Mitigation:
    - Kernel needs its own address space
    - Each CPU has its own kernel local address space
    - Some parts of the kernel still needs mapping into the user process (but limited)
- Performance impact:
    - 30% Context switching overhead
    - 57% Thread switching overhead
    - No impact on multi-cpu iperf3
    - 1.6x single-cpu iperf3

## Spectre
- Indirect branch prediction speculatively access data causing side effects
- Most variants do not work across process boundaries
- Usually code execution required to train the CPU branch predictor
- Mitigations:
    - Intel microcode update
    - Full prediction barrier at context switch
    - Indirect branch prediction barrier at kernel entry
- Benchmarks are very bad
    - Worse than a 10-fold increase in IPC round-trip time (!)
    - Raw packet throughput down 20%
    - 1CPU packet throughput down 75% (!)

## Spectre NG
- Speculative access to FPU state while current context is not the owner
- Bad because L4Re uses lazy FPU switching
    - Don't save & restore FPU state for threads which don't need it
- Mitigation:
    - Eager FPU switching
- Performance loss is 4%

## Foreshadow (L1 Terminal Fault)
- CPU speculatively invalidated page table entries
- Affects OS/SMM, VT-x, SGX
- L4Re doesn't support SGX
- SMM needs to protect itself
- OS
    - L4Re not vulnerable because invalidated page table entries are zeroed-out
- VT-x is nasty
    - Guest can provide a malicious page table
    - Microcode update
        - New MSR and new instruction for L1D flush
- Flush L1D on every vmresume

## One more thing
- Mitigations are configurable
