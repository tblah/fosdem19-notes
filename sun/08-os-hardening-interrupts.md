# 16:05 - 16:30 Hardening External Interrupts (AW1.121) - M P Tokponnon

## Transient faults
- Ionising particles can change the state of a logic circuit

## Objective
- Hypervisor-based hardening (Nova + Genode + Virtualbox)

## Methodology
- Memory is protected in hardware (ECC)
- Hardware detects memory faults and triggers an interrupt
- External interrupts do not happen immediately. They are delayed until the end of the processing unit (~ sequence of instructions without a context switch (maxed at ~200 instructions))

## Interrupt delayed
- 323 Gcycle
- 21% timer interrupts delayed
- No other interrupts are delayed

## Conclusion
- When double executing process instructions, external interrupts servicing is delayed to preserve idempotency among the two runs
- We investigate this interrupt delaying impact on process execution in Genode Demonstration...

## Questions
- A processing element sounds like it can only work on a single processor system. Is that true?
    - Yes
- Is it dangerous to disable interrupts for that long?

I didn't really follow that.
