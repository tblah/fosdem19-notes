# 13:00 - 13:35 Unikraft: Unikernels made easy (AW1.121) - Simon Kuenzer

## What is Unikraft
- Open source Unikernel from NEC

## VMs vs Containers
- People like containers
    - Easy to create
    - Much smaller
    - Boots way faster
- VM advantages
    - Strong isolation
- Unikernels
    - Get both?

## Unikernels as VMs
- Purpose built thin kernel layer containing only what the application needs
- No isolation within the unikernel (isolation is done by the hypervisor)

## Unikernel gains
- Fast instantiation, destruction and migration
    - less than 10ms for a native process
    - 150ms to 600ms Docker
    - 63ms to 1.4s Unikernel
- Low memory footprint
- High density (10k guests on a single node)
- 10-40Gb/s throughput with a single guest CPU
- Very good at huge numbers of requests per second
- Reduced attack surface
- Strong isolation provided by hypervisor

## Unikernel applications
- Fast boot, migration, deploy (serverless (tm))
- Resource efficient (IoT)
- High performance
- Mission critical (small code base -> security)

## So why isn't everybody using Unikernels?
- Optimised unikernels are manually built
    - Takes a lot of work
    - Have to write new drivers for each hypervisor
    - Redo for each application
- Enter Unikraft...

## Unikraft
- Support a wide range of use cases
- Simplify building and optimizing
- Simplify porting applications
- Common shared code base for Unikernel creators
- Support different hypervisors and CPU architectures
- "Everything is a library"
- Two components:
    - Library pool
    - Build tool

## Library pool
- Main libs
    - Schedulers, network stacks, filesystems, runtimes
    - Several choices of each
- Platform libs
    -  Support Xen, KVM, etc
- Architecture libs
    - Hardware support

## Example: Unikernelise a python script
- Select micropython, network stack, arch, etc
- make

## The build tool
- Kconfig-based
- `make menuconfig`
- ...

## A baseline example
- Xen PV x86_64 binary
    - 32.7kB image (mostly libxenplat.o
    - Boots and prints hello world

## Upcoming 0.3 version
- Support for
    - Xen
    - KVM
    - Linux userspace: x86_64, Arm32
    - Bare-metal: x86_64 (KVM target)
- Core functionality
    - Cooperative scheduler
    - Binary buddy heap
- Networking
    - Low level API for high-speed IO
    - virtio-net

## Road-map
- ARM64 support
- More standard libraries
    - musl, libuv, zlib, openssl, libunwind, libaxtls
- Language runtimes
    - Javascript, Python, Ruby, C++
- OCI container target support
- Filesystems
    - In-RAM and virtual disk filesystems
- Network driers
    - Xen (netfront), Linux (tap)
- Frameworks
    - Node.js, PyTorch, IntelDPDK

## Demo
- Development process for unikraft programs is a lot like writing Linux Kernel modules
- Static http server
- 222kB VM

## Questions
- Is everything run in kernel-space?
    - Yes. POSIX system calls are function calls
- How hard is it to port Linux drivers?
    - Pretty hard: they use a BSD licence. They are looking into porting BSD drivers though.
