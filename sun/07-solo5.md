# 14:30 - 15:15 Solo5: A sandboxed, retargetable execution environment for unikernels (AW1.121)

## What is an unikernel?
- Roughly a minimalistic library operating system
- Generally no process isolation
- Generally co-operative scheduling
- Minimal code size, *minimal attack surface*
- It doesn't have to run in kernel space
- Think of it as a process which does operating-system kind of things

## What is Solo5?
- Interface between the VM host and the Unikernel
- A minimalist, legacy-free interface
- Bindings to this interface for
    - Microkernels (genode)
    - Separation kernels (Muen)
    - Virtio-based hypervisors
    - Monolithic Kernels (Linux, FreeBSD, OpenBSD)
- It is middleware, integrated into libOS buid system
    - The developer does not interact with Solo5 directly

## Solo5 Compared
- Much smaller interface than VM/hypervisor or container/host

## Philosophy
- Minimal
    - Simplest useful abstraction
    - No device discovery at runtime
    - Typical configuration ~3k LoC
    - 12k LoC total
    - Clarity of implementation
    - Fast startup (less than 50ms for hvt/spt)
- Stateless
    - Very little state in the interface itself
        - No dynamic resource allocation
    - Host cannot change guest state
        - No interrupts (!?)
    - Results in a system that
        - Is deterministic
        - Is static
        - Enables strong isolation
- Portable
    - MirageOS (Ocaml), IncludeOS (C++), Rumprun (NetBSD)
    - KVM, Bhyve, OpenBSD vmm, Muen Separation Kernel, Genode OS framework
        - Not contributed by solo5 "experts"
- The implementation should do one thing and do it well
    - Be an engine for running unikernels

## Limitations
- Minimal
    - Does not run Linux applications
        - But there are POSIX-ish libOSes (Rumprun, LKL) that do
- Stateless
    - "No interrupts" implies single core
    - Not for interfacing to hardware
    - Drivers are "some other component's" problem
- Portable
    - Performance (copying semantics, number of "calls per IOP")

## The Interface
- `solo5_app_main`
    - cmdline string
    - heap start
    - heap size
- `solo5_exit`
- `solo5_abort`
- `solo5_console_write`
- `solo5_clock_monotonic`
- `solo5_clock_wall`
- `solo5_yield`
    - Please yeild to the host until some deadline comes
- `solo5_block_read`
    - offset, buf, bufsize
- `solo5_block_write`
- `solo5_block_ifo`
- `solo5_net_read`
    - buf, bufsize, read size
- `solo5_net_write`
- `solo5_net_info`

## The Interface (future)
- Device handles so you can have more than one block and network device

## hvt "Hardware virtualized tender"
- Uses hardware virtualization as an isolation layer
    - KVM, Bhyve, vmm
    - 10 hypercalls
    - Modular, typical configuration: ~1.5k kLOC
- Loads the unikernel
- Sets up host resources
- Sets up VCPU, page tables
- Handles guest hypercalls
- x86_64 and ARM64

## spt "Sandboxed process tender"
- Uses process isolation with seccomp-BPF as an isolation layer
- The system call filter is a *strict whitelist*
- ~7 system calls needed for the entire Solo5 interface
- Should be possible to port to FreeBSD and OpenBSD
- Loads the unikernel
- Sets up host resources
- Applies the seccomp-BPF sandbox
- Treats the hosting Linux kernel as a hypervisor
- Directly invokes syscalls (no libc)
- x86_64 and ARM64 (trivial to add more)

## muen: Native component
- A separation kernel for high assurance
- Gaurentees that components communicate exclusively according to given security policy
- Isolation using hardware virtualisation
- Implemented in ADA/SPARK
- *Formally proven to contain no runtime errors at the source code level*

## Debugging
- Just use GDB.

## Future
- Security
    - ASLR, SSP, W^X
        - Undocumented ABIs
        - Hypervisor support lacking on FreeBSD and OpenBSD
            - EPT mprotect
    - Defence in depth
- Portability
    - Can we do dynamic linking safely?
- More languages
    - Go
    - Rust

## Conclusion
- Minimalism!
    - 13 functions. ISC licensed.
    - Low resource usage. 1500 VMs on a 3 year old laptop
- https://github.com/solo5/solo5

## Questions
- Can you run solo5 spt on solo5 svt?
    - No. We haddn't thought of that. Sounds interesting?
- Can this be used to sandbox desktop graphical applications. Is this possible?
    - What would that interface be? For performance you need at least a frame-buffer
    - You could run an X11 client inside this thing and talk to an X-server somewhere else
    - But nothing that good. See Genode
