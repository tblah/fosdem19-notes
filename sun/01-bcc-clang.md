# 09:45 - 10:25 Rewriting Pointer Dereferences in bcc with Clang (K.4.201)

## What is bcc?
- Tools for compiling BPF programs
- BPF is a bytecode VM in the Linux Kernel
    - Statically verified at load-time to prevent crashes
    - Can call some functions outside the VM

## Tracing tools in bcc
- You can aggregate data within the kernel before sending to userspace

## Updating BPF in the Kernel
1. Compile from C to BPF using Clang
2. Load BPF program into the kernel
3. Kernel verifies BPF program
4. Create maps for BPF program (access data outside the BPF VM)
5. Attach BPF program to hook point

## Clang Rewriter in bcc
- Source-to-source translation
- Used for many things
    - Parse map declarations and create them
    - Parse function names and attach program correspondingly
    - Rewrite function declarations
    - Rewrite map accesses
    - Replace dereferences of pointers to kernel memory (focus of talk)

## Reading Kernel Memory from the BPF VM
- Memory access can't be verified statically
- Use external function to read kernel memory
    - Allows runtime checks
- When writing using bcc you can use dereferences as usual
    - BCC rewrites dereferences to use dereferencing function call

## Doesn't aim to be perfect
- We don't always need this rewriting
    - False positives: call `bpf_probe_read` when we were just accessing VM mem
    - False negatives: program will be rejected by the kernel verifier

## Sources of external pointers
- The context argument
- From calls to external functions

## Traversing the AST
- We need to track all external pointers through the code
    - Follow function calls
    - Update set of external pointers as we go
- Second pass doing the rewriting

## Tracking levels of indirections
- Pointers to external pointers
- We don't want to rewrite the first dereference but do want the second

## Tracking external pointers through maps
- External pointers might be stored in maps

## Conclusion
- Rewriting external pointers at source-level is a pain
    - Requires several AST traversals
    - Despite all this complexity it is not complete and probably won't ever be
- Better approaches?
    - Rewrite at bytecode level?
    - Rewrite all structures form kernel headers?
    - Ask developer to label external pointers
    - No rewriting at all?

## Questions
- Could you use compiler attributes to do this? Like user pointers in the kernel
    - Yes, but we are trying to keep the user from having to care about this
- Have you tried to detect these external pointers at the IR-level?
    - It would not be popular upstream so we haven't tried it
- How do you guard against access to uninitialised memory?
    - This is checked at runtime
- Does this work for all kernel structures?
    - Yes
- Could you do this with LLVM IR Address Namespaces?
    - I don't know
- Why don't you use something easier than C as the input language?
    - Kernel devs are used to using C. There is a Lua front-end though.
