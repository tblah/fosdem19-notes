# 10:40 - 11:00 A roadmap for the Hurd (AW1.121) - Samuel Thibault

## The HURD is all about freedom #0
- "The freedom to run the program, for any purpose"
- The users shouldn't have to ask the sysadmin for permission to do things
    - Users should be able to run a VPN
    - Users should be able to store their data on an experimental filesystem
        - Without crashing the system
- Freedom to innovate
    - Experimental drivers can crash without taking out the system

## Micro-kernel layering
- Kernel
    - Tasks
    - Memory
    - IPC
    - Disk drivers (but not for much longer)
- Root
    - pfinet
    - proc
        - handle processes
    - auth
        - authentication
    - ext2fs
- user
    - sh
    - cp
- Server crach? Not a problem
- Easy to debug
    - Just run gdb, gprof
- Dare to do crazy things
- The Hurd console has dynamic font support
- Namespacing deep into the structure of the operating system
    - You cannot forget to compartmentalise a subsystem because it is inherent
      in the design
- You can do clever things with it like browsing an iso over ftp without
  downloading the whole iso

## Current state
- Quite stable
    - Have not reinstalled boxes for a decade
    - Debian build machines. No hang after weeks!
- 75% of Debian archive builds out of tree
    - Incl. XFCE, gnome, KDE
- Support merged upstream:
    - gcc, glibc, llvm
- Ongoing:
    - go, rust
    - GuixSD, Arch
- Debian distribution

## Roadmap
- Needs polish
    - 10% time you get 90%. 90% of time to get the last 10%.
- Bits to polish
    - httpfs, ftpfs
        - Some http index pages don't parse correctly
    - Namespace-based translators (see commas)
        - `cd software.tar.gz,,/`
        - `cat README`
        - `cd disk.img,,part1,,ext2fs/`
    - mboxfs
    - xmlfs
- Ongoing work
    - PCI arbiter
        - Safe concurrent access to PCI config space
        - Use an IO-MMU
        - Allow a process to control a single PCI device
    - ACPI translator
        - Support ACPI operations e.g. system shutdown
    - Rump translator
    - 64-bit kernel support
        - Then 64-bit userland support
    - SMP support

## No driver in the kernel
- Kernel provides tasks, memory, IPC
    - Currently also disk drivers. This needs moving to userland
        - How do you boot the system?
        - A GRUB module?

## Conclusion
- It is fun to hack something but it is better to finish it
- Getting a whole distro running is hard work

## Questions
- What improved in the past 3 years?
    - PCI arbiter
    - ACPI
    - There is nobody working full-time on the Hurd so progress is slow
