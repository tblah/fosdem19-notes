# 11:10 - 11:55 RedoxOS (AW1.121) - Robin Randhawa
Porting Redox OS to ARM

## Intro
- Speaker works at ARM open source system software group
    - *Specialises in  open source safety-critical systems, especially automotovive*
- Talk about requirements for autonomous vehicles
    - Many of the worst issues come from  C
        - Buffer overflows, integer overflow, etc
    - Current solutions
        - MISRA
        - Formal verification of hardware and software
            - Gold standard but not really practical for complex systems
        - Rust!

## Rust
- Do fancy things while remaining safe
- Highly expressive
- Performance benchmarks
    - "The performance of machine code generated from idiomatic Rust is typically at par or better than machine code generated from idiomatic C++"
- ARM are encountering a lot of industry interest in rust
- Rust prevents lots of common errors
    - Can't forget to initialise variables
    - Can't overflow an array
    - Can't forget to free data
    - Can't forget to take locks
    - You cannot have a dangling pointer
    - Can't double-free
    - Can't use after free
    - All checked at compile-time
- Safe and unsafe rust
    - Unsafe is opting out of the extra checking to write C and C++ like code
    - You don't need unsafe most of the time
- Before compilation succeeds, rust requires the programmer to
    - Acknowledge any possibility of error
    - Take some suitable action
- No exception handling
- Data immutable by default
- Rust has (generally) less ambiguity than C
- You have generics
- You have atomics (following LLVM's C11 model)
- Good FFI

## Rust and Redox
- Rust was very useful for writing a microkernel
- unsafe rust made it easy to locate bugs

## Redox
- Microkernel in Rust
- Graphical
- POSIX compliant C library
- Simple web browser (which doesn't work well)
- MIT licensed
- Implements Unix syscalls
- Showcase safe and secure software development using Rust
- Redox written by Jeremy Soller (System76)
- Inspired by Plan9 with a twist
    - Everything is a URL (not a file)
    - e.g. access a usb device: "usb0://"
- Processes can be put into a "null" name space (containers)
- 8k-9k lines of code
- No virtualisation support

## Porting Redox to ARM
- Targeting qemu's AARCH64 machine
- Most of the way there but not quite got feature-parity with amd64
- It runs Doom

## Conclusion
- Redox has made a lot of progress very quickly.
    - Maybe there is something to be learnt here
