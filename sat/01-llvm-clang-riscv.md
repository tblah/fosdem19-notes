# 10:30 - 11:15 LLVM+Clang for RISC-V (AW1.126) - Alex Bradbury
## What is LLVM?
- Apache
- Library
- Used as a backend
- Common infrastructure
    - Code generation
    - Linker
    - Debugger
    - Runtimes

## Why LLVM?
- GCC for RISC-V is best supported
- But LLVM is useful because it is used as a backend for rust
- Unis like LLVM

## Compilation flow
c input -> LLVM IR -> Selection DAG
- LLVM IR is target-specific, although not much
- Selection DAG: much more target specific

## Describing ADD
- LLVM DSL TableGen
- Verbose and hard to maintain so you create a class hierarchy of instructions

## Targeting RISC-V
- RISC-V has a whole family of variants & extensions
- These extensions have a whole range of ABIs
    - Hard/soft floating point
    - Double, single floating point
    - More to come

## History
- Funding over the last 18 months
- LLVM code review used to take a very long time but this is improving
- "It is not there yet for hardfloat linux targets"
    - Targeting next release of LLVM
    - But multiple companies are using it internally for embedded 32-bit builds

## Compressed extension
- In the standard ABI every instruction is 32 bits
- If you support the compressed extension then a subset of the instructions can
  be encoded in 16-bits
    - Similar to Thumb2
- These 16-bit instructions have limitations, e.g. limited register access
- Handled as a late-stage conversion (generate in the 32-bit instructions first
  then convert to 16-bit)
    - This makes the code much simpler
    - But the register allocator has been taught about this so that it can prefer
      the 16-bit accessable registers
    - Not much interaction with instruction selection
    - Work to be done here

## RISC-V Memory Model
- Relaxed memory model not dissimilar to AARCH64
- C11 atomic add, atomic compare, etc
- Look at what the code generator does, then convert it to what it should be
- This turned out to have some complex restrictions
    - (regarding pipeline hazards(?))
- Treat compare-exchange as a black-box when it is lowered:
    - Don't convert to RISC-V instructions until after everything else is done
    - Avoids spills

## Testing
- Mostly unit testing
    - LLVM has an infrastructure for this for IR -> ASM
- Testing SPEC programs
- Qualcomm work on fuzzing the assembly parsing
    - Where is GCC and LLVM different?

## Linker Relaxation
- LLVM has fairly small immediate sizes
- RISC-V needs to load instruction addresses in two parts (20 + 12 bits)
- Linker relaxation is figuring out when you only need the 12-bit address offset
    - Thus removing an instruction
- TODO

## ABI Lowering
- There are a lot of ABIs (extensions) but they are quite similar
- "ABI Cop" - test conformance to function call semantics
- This has to be repeated for every language (CLANG, Rust, Julia, Swift, etc)

## Status
- Contributions from quite a few companies
- Some limitations
    - TLS support isn't upstreamed yet
    - Position independent code TODO
    - Hard float ABIs TODO
        - 64-bit float code-gen merged a few days ago
- Rust port
    - Active community
- LLD
    - It works but is early days
- TODO RISC-V Vector extension
- TODO Improving code-compression (see later talk)
    - CLANG's re-ordering of basic blocks is quite aggressive compared to GCC
        - Ensure hot path is linear in memory
        - Fast but bad for code-size because the branch offsets become longer
- TODO Bitmanip extension
    - Instructions for bit manipulation
    - Not standardised yet but aiming for compiler support first to provide
      feedback to the standardisation effort
- TODO Support for floating point instructions on general purpose registers
- TODO Support for target-independent LLVM features
    - MachineOutliner (opposite of inlining)
        - Bad for performance but great for codesize
- TODO structured approach to benchmarks

## Tutorial
- Getting started hacking on LLVM http://lowrisc.org/llvm/devmtg18

## Questions
- How much work is architecture support for language frontends?
    - Because ABI is not target independent
    - It depends upon the architecture & ABI
    - The testing can be quite a bit of work
    - Work ongoing on this

