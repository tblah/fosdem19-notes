# 15:20 - 15:50 Reduced Binary sized bootstrap for GuixSD (K.4.201) - Andy Wingo

## Summary
- Much success

## Guile 1.6 (2006)
- Expand and interpret. Repeat

## Guile in 2010
- Expand, optimise, codegen, run byte-code
- Interpreter is still a virtual machine

## Guile in 2017
- Expand, optimize, lower, optimize, codegen, lower-level-bytecode
- "CPS soup" lower level bytecode

## Current Guile needs
- Approach Racket
- Make Guile faster
- Guile's compiler is written in guile -> speed up compilation
    - Especially for Guix

## Guile 3: 2019
- Compile bytecode into machine-code
- The aim is for the bytecode to be as low as possible while remaining general
    - Do less with each bytecode instruction
        - More instructions
        - More control flow
        - More work for optimizer
        - It is not clear that this will be an improvment:
            - Overhead per bytecode instruction
            - Hopefully compiling to machine code makes up for this

## Code Generation
- GNU Lightning
    - Supports loads of architectures
- Native code performs the same operations on Guile stack that VM interpreter would
    - They are interchangeable at runtime as we spot hot paths
- When?
    - Ahead of time e.g. GCC. This is possible but is not yet implemented.
    - JIT: This is what we do currently.
        - At some point we decide that compiling is a good idea
        - Tradeoff with startup time
        - Per function counter incremented at call and loop iteration
            - Configurable tier-up threshold

## Status & TODO
- GNU Lightning impedance problems
    - Looking at replacing GNU Lightning
- Code quality is poor
    - Register allocation is poor
- Get performance comparable to Chez
- WASM backend
    - Depends on WASM GC proposal
- Racket-ification

## Questions
- A while ago Guix changed garbage collector. Opinions?
    - It was the right choice at the time
- Racket is rebasing onto Chez, why don't you?
    - The author enjoys compiler work
- Do you have ideas how to write a register allocator?
    - Not really. We have some ideas for a calling convention.
