# 16:20 - 16:50 A Guiler's Year of Racket (K.4.201) - Christopher Lemmer Webber

## Getting into Racket
- No mutable top level in racket :(
- Racket has a picture language which is less intimidating to new people
- Racket scribble was useful for humanities students who needed to write papers
- Delivering workshops
- Racket is easy to start new projects (no autotools)
- Docs are good
- High quality GUI library just works and is cross-platform
- Loads of libraries in racket
- "It is like the python of lisp"
- DrRacket graphical editor is good, especially for people who don't know emacs

## What about that immutable toplevel?
- Not so bad. REPL feels the same most of the time
    - This even prevents some live hacking error too
    - You remember to copy code back out of the repl
- But it is still a shame "the toplevel is hopeless"
- Racket is open to supporting a mutable top level in the future

## Guix advantages
- Guix
- Mes, Chickadee, etc
- Bootstrapping

## Guix disadvantages
- Too many libraries to catch up to
- Accessibility for non-emacs people

## What if Guix on Racket?
- Heresy
- More contributors
- GUIs

## What can Guix do
- Package more Racket packages in Guix?
- Build a Guix GUI in Racket?
- What if Racket was built on Guile instead of Chez

## Conclusion
- Racket is the new primary residence
- But not leaving Guile
- Projects should work together

## Questions
- Is racket embeddable?
    - Maintained but not so good as Guile
    - It is only getting worse
