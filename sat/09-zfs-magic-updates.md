# 17:00 - 17:40 ZFS Powered magic Upgrades - Alan Jude (K.3.401)

## Before ZFS: NanoBSD
- Divides the disk into two partitions :(
- Small 3rd partition for config
- Install stock on both
- At upgrade time, overwrite the inactive image
- Boot once to the newer image. If it fails, or is otherwise unserviceable, reboot to good image
- If the image is accepted. Set it as the new default

## New: ZFS Boot Environments
- Many file systems with no need to partition your disk
- Separate the OS / from user data, /home, logs, etc)
    - All can be different partitions unlike the sneaky bind mounts for NanoBSD
- Snapshots and clones are instantaneous and only store diffs
- Bootloader menu selects which version of the root filesystem you want to use
- You can easily choose if /usr/local is in the OS or user

## Default ZFS partition layout
- Installer creates zroot/ROOT/default as the default boot environment
- We don't actually mount /usr so that those files fall through into /
    - But /usr/obj is user data so compiles can be resumed
- /home isn't OS
- /var isn't mounted so that that stuff ends up in the OS
    - But /var/crash, /var/audit, etc are all user

## What about /etc? Configuration file merging
- Make /etc its own filesystem?
    - /etc/rc, /etc/rc.conf, /etc/ttys and /etc/fstab are in /etc so this needs to be in the rootfs
- But we really really don't want to mergemaster
- loader.conf variable init_script
    - Intended as a script to run first to set up a chroot
    - Use this to mount /cfg while we are still in single user mode (before we neeed /etc)
    - Replace persistent files in /etc with /cfg
- There are about 10 important files that end up /cfg
    - Network settings, sysctls, SSHd keys, fstab, etc
- Never have to merge /etc/rc.d files again

## How to deploy a boot environment?
- Create an image
    - zfs snapshot img/ROOT/be@snap
    - zfs send -pec img/ROOT/be@snap | xz -9 > bename.zfs.xz
- Apply the image
    - fetch -o - https://svr/bename.zfs.xz | unxz | zfs recv
- Goot once
    - zfsbootcfg zfs:zroot/ROOT/newbe

## Shortcomings
- Fragile
- Original install couldn't be like this

## Using BEs at Scale
- 100 servers, 38 DCs, 11 countries, 4 FreeBSD versions
- freebsd-update upgrade too manual
- zfs recv; zfsbootcfg; reboot takes less than a minute
- Failure is graceful. Cycle power and it is fixed
- Remote upgrades with confidence, even without console access
    - Especially for servers on the other side of the world

## Build the images faster
- Poudriere is used to build official FreeBSD binary packages
- 1 jail per core, one package in each jail, only dependencies installed, no network
- Ensures you don't introduce undeclared dependencies
- You can build your own ports tree for each freebsd version you want

## Even better
- poudriere image
- Creates customized VM and USB images. Used to build FreeBSD images for public cloud
- Add ZFS send as a file format for this
- WIP: see github.com/allanjude/poudriere
- Create from releases without having to compile everything

## Poudriere TODOs
- Support more image types
- Rename things to be clearer
- Integrate cloudware e.g. BSD cloudinit
- Use this for FreeBSD Release building
- Suppoert for post-build scripts (chroot)
- Anything else?

## Questions
- How does zfsbootcfg work with Solaris?
    - It writes to a reserved area so that Solaris isn't confused but it doesn't support it
- Can you stop yourself accidentally saving things onto / and then rolling it back
    - Read-only / or very small quotas
- ZFS feature changes between FreeBSD releases
    - Don't run zpool upgrade until you are sure you don't need to rollback
- Why not do a union mount?
    - FreeBSD doesn't have a good union filesystem which does not crash
