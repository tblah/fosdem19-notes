# 14:05 - 14:40 FreeBSD Graphics (K.3.401) - Niclas Zeising
- `github.com/FreeBSDDesktop`

## The Graphics Stack
- Core Libraries
- X Servers
- Wayland
- Applications

## Legacy Graphics Driver
- Complete port from Linux 3.8
- To be removed in FreeBSD 13
    - Because they conflict with the new drivers
- To be replaced by drm-legacy-kmod port

## New Drivers in Ports
- Based on Linux Kernel Drivers
    - Use Linux KPI (translates Linux kernel APIs to FreeBSD)
        - As few changes as possible from Linux
- Never will be in base
    - Licensing
    - Different release cycle
- amd64 tier 1
    - arm, i386 and ppc64 secondary
- Multiple versions (for different freebsd versions)
    - Use the meta-port graphics/drm-kmod
    - Available for 11.2 and 12.0

## Graphics Libraries
- mesa (OpenGL)
- libdrm
    - Translation from userland to kernel drivers (for hardware independence)
- xorg libraries

## X Server
- out of date :(
- Two methods of communicating with graphics hardware
    - DDX Drivers (old `xf86-video-*`)
        - Device dependent
    - modesetting
        - Upstream seem to be going in this direction

## Wayland
- Up to date
- Support in GTK, Qt, Mesa in ports
- Sway is in ports
- evdev support in kernel needed
    - In FreeBSD but not in the default kernel config on older FreeBSD
    - Needs more testing

## Getting Started
- Used FreeBSD Stable or Current
- `pkg install xorg`
- `pkg install drm-kmod`
    - If you are running on `-CURRENT` then build this form ports
- `startx`
- Works well on Intel
- AMD might need some tinkering

## Challenges
- **Testing**
    - It is hard to emulate graphics hardware
    - There is a lot of hardware variety
    - Help please
- Build time
- Developer bandwidth
    - Upstream moves fast

## Future
- libinput is changing in Linux
    - This will need some sort of translation layer
        - evdev kernel code
- drm-legacy
    - Should it live forever?
- Autoloading of the kernel driver (Linux drivers look a bit different to FreeBSD)
- More architectures

## Further into the future
- Network manager
- Power management
- WiFi support
    - Pretty good today but there is always a long tail

## Questions
- Wayland and Nvidia?
    - Probably not
- Optimus support
    - Does that even work on Linux?
    - Nobody in the graphics team has the hardware
