# ELI5: ZFS Caching - Alan Jude (K.1.105 La Fontaine)

## What is ZFS
- FS with built in volume manager
    - Filesystems are thin-provisioned on top of the pool
    - Or we can just have block volumes (zvols)
- Every block is checksummed
- Transparent compression
- Copy-on-write
    - FS always consistent
    - No performance penalty to snapshots
        - Clones allow you to fork the filesystem
        - Snapshots only take the space for a diff

## Caching
- Memory hierarchies
- Most caches are LRU (Least Recently Used)
- LFU (Least Frequently Used): cache is a sorted list
    - Immune to a scanning issue (e.g. read a whole database without loosing
      woring set)
    - Hard to get old things out of the cache as the working set changes

## The ARC
- There are 4 lists: LRU, LFU, and their ghost lists
- Ghost lists are lists of recently evicted blocks
- Adjust relative sizes of LRU and LFU caches using information from ghost lists
    - Every time we have a miss that would have been a hit if the LRU was bigger
      (it was in the LRU ghost list) then the LRU is grown and LFU is shrugged)
        - and likewise
- So the ARC gets a better hit rate than either an LRU or LFU
- In addition, the whole size of the ARC can grow and shrink over the time
    - Computers do things other than filesystems

## The L2 ARC
- When you are running out of memory you can use some very fast storage for L2

## Compressed ARC
- Don't bother compressing things with gains less than 12%
- There is a small cache of decompressed blocks
- Takes advantage of compression being done already

## Cached metadata
- ZFS needs a lot of metadata (e.g. block hashes)
- By default metadata can't use more than 1/4 of the cache
    - So that some data actually gets to go into the cache

## Tuning
- Increase metadata cache proportion?
    - Good for rsync
    - metadata compresses very well
- Fileserver & block storage
    - Large arc, for block storage make sure the block sizes match
- Double caching
    - Some databases run best when the ARC doesn't cache data at all
        - Database sometimes knows better what is going on and can cache better
            - But with compression sometimes it is much better to cache in ARC
    - VMs: small-medium ARC, reserve memory for VMs (don't fight with ARC)

## Questions
- Would you recommend ZFS for desktop users?
    - See my 5pm talk :)
    - FreeBSD boot environments:
        - Snapshot before upgrades, choose versions in FreeBSD boot menu
        - thin-provisioned filesystems makes it easy to have /home separate
- How does the cache deal with mmap?
    - As pages fault they are copied from the ARC
    - This can lead to double-caching, even without compression
    - The ARC has to be read-only so mmap and zfs don't play well
- Optimisation for memory technologies other than SSDs
    - Tunables controlling write speed to L2 ARC
        - Rate limiting for bandwidth R/W tradeoff
        - Device write lifetime
- How are ghost lists managed?
    - LRU
- How is ZSTD support going?
    - ZSTD has varying compression levels
    - First round of the implementation is done
        - How do you handle updates to ZSTD algorithms vs checksums?
- Windows VM on ZFS
    - L2 ARC bigger than VM
    - But disk still getting hit
    - Why?
    - The fill-rate tunables limiting write-rate to L2 ARC
- Turn compression on before you write any data
- How to configure ZFS for NVMe
    - The biggest difference is that you can run multiple commands in parallel
        - Big performance gain from increasing the queue sizes
