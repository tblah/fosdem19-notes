# 15:20 - 15:50 Reduced Binary sized bootstrap for GuixSD (K.4.201) - janneke@gnu.org

## What is GNU Mes?
- A Scheme interpreter written in ~5000LOC of C
- A C Compiler written in Scheme

## What is a Binary Seed?
- A program that was not built from source
- A program that was injected from a previous generation
    - Binutils, GCC, Glibc, Haskell, Java, Perl, Rust, ...

## What is a Bootstrap Seed?
- Guix 0.1.6 ~250MB bootstrap binaries
    - bash, binutils, bzip2, coreutils, etc
- Debian ~450MB
- Guix using Mes: ~130MB

## What is a bootstrap?
- GCC source + ??? = GCC binary

## Why?
- Ken Thompson's "Reflection on trusting trust"
- Big binary blobs are non-audible
- GPLv3

## 500 byte hex0 monitor
- bootstrap itself in 500 bytes

## Tiny CC
- TCC is a very small C compiler which can compile GCC

## Mes+MesCC
- Use a small binary blob to build Mes (a minimal Guile Scheme)
- MesCC is a C compiler in Mes scheme

## Aim for the stars:
- Stage 0
    - hex0-assembler -> hex1-assembler (M1 Macros)
- Stage 1
    - Simplified C (M2) compiler in M1 Macros
- Stage 2
    - Build Mes using simplified C compiler (TODO)
- Use Mes to build the rest

## In progress
- Scheme only bootstrap (Gash)
    - Busybox in scheme
- Mes v0.20: Mes C Lib support for awk, bash, sed, tar
- Bootstrap Mes.M2 using M2-Planet
- A Reduced Binary Seed bootstrap for Nix.
- Build GCC-4.x directly (skipping gcc-2.95.3)

- bootstrappable.org

## Questions
- How can people help? Low-hanging fruit
    - IRC/Web/Email say hi
    - Guix developers can help get this into mainline
    - Expand for other architectures (ARM requested)
    - Documentation
    - Improve Gash
- Funding? EU Funding Suggested
    - No funding right now, applied yesterday, chances don't look good
    - Yes please
- What is the point when the management engine runs Minix?
    - Hopefully somebody else will fix this
    - Power9?
        - Yes please. Please help!
