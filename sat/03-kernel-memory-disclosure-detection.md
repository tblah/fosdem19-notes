# 13:25 - 13:55 KLEAK (K.3.401) - Thomas Barabosch

## Syscall Recap
- Program calls syscall N
- Syscall dispatcher figures out which function to call
- Syscall handled
- Return to userspace

## Kernel/User Space Data Exchage
- Trust boundary between userspace and userspace
- How to copy data safely from the kernel address space to userspace
- `copyout`, `copyoutstr`

## What are kernel memory disclosures
- Inadvertently writing data from kernel to user space
- Leak kernel pointers (breaks KASLR)
- Leak keys/tokens
- Typically doesn't directly leave to privilege escalation but it is an important step
- e.g. CVE-2018-17155 FreeBSD-EN-18:12.mem
    - Allocates struct
    - Assigns to values in struct
    - Struct padding bytes leaks uninitialised kernel data
    - **Always memset**

## Why are these hard to detect?
- Silent bugs: no crashes
- Syscalls often hidden behind C library
- Compilers don't pick it up
- Architecture dependent
- Developers may not be aware of the issue

## Typical errors
- **See Mateusz Jurczyk's publication about BochsPwn Reloaded**
- C
    - Uninitialised variables
    - Struct alignment
    - Padding bytes in struct
    - Unions are evil

## How to avoid it
- Local variables are uninitialised
- Heap memory uninitialised
- Don't trust the compiler
- Don't assume any padding
- Be very careful with structs & unions: initialize asap
- When developing a new syscall:
    - Dump the exchanged buffer in userspace and check for leaked bytes
- When in doubt zero out
    - One byte can break KASLR

## KLEAK
- KLEAK is automated tooling for detecting kernel memory leaks
- Memory tagging
    - Memset huge stack area with a marker byte
    - Patched heap always initialises with marker byte
- On `copyout` check for the marker byte
    - Write down into a hitmap
    - Try different rounds with different marker bytes
        - Decreases false positives

## Details
- Instrument memory allocator to `memset` marked chunks
    - Exception: requests for zero'ed chunks
- Function to taint the whole stack
    - Problem: the syscall functions add to the stack
    - Solution: compiler instrumentation to continually re-taint stack
- Detection only in `copyout`. Don't track data through the kernel

## Choice of marker values
- Estimate byte frequency and pick something rare
    - Measured using patch to `copyout`, `copyoutstr`
    - `copyout` mostly uniform but `copyoutstr` limited to ascii chars

## Results
- 0-days found in NetBSD and FreeBSD

## Questions
- How expensive would it be to use compiler extensions to zero everything always?
    - This exists (non-standard) on Linux
    - This doesn't cover all sorts of leaks
