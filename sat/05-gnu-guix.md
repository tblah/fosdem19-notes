# 14:50 - 15:20 Building Distro on Top of a Minimalistic Language (K.4.201) - Ludovic Courtes

## Intro
- GNU comes in many languages
- This raises the bar for anyone who wants to learn

## GUIX
- Guix is a package manager
- See earlier talk
- Create docker images & application bundles
- Distribution
- Minimalism
    - See GNU Guix Reference Card
- Reproducible

## Distro Management
- Declare system configuration entirely in one scheme file
- Systems can be built from the config file
    - Containers
    - Install on hardware
    - VM

## How did we get here?
- Everything has its own configuration languages
- Adding a `for` keyword to a domain specific config language becomes a feature
- Solution: just do everything in LISP

"The truth is that Lisp is not the right language for any particular problem.
Rather, Lisp encourages one to attack a new problem by *implementating new
languages* tailored to that problem" - Abelson & Sussman, 1987

## GUIX SD
- Scheme bindings for configuration files

## Unification beyond the "distro"
- Nix uses a DSL to describe packages with snippets of bash where the DSL lacks
    - Quoting issues, especially inserting variables into bash strings
- Instead GUIX uses Scheme, *which is a general purpose language*.
    - This way we can do everything in scheme, which doesn't have these problems
    - Intiramfs and bootloader are in Guile Scheme so you can do everything like that
    - Lots of nice code reuse

## Summary
- Distro & Tools as a Scheme library
- Hackability through uniformity
    - Everything is in one git repo
- GUIX SD 1.0 coming very soon

## Questions
- How do we generate scheme bindings for configuration files?
    - Most config files look like `key: value`. There is a library for that.
- Atomic upgrades?
    - Yes
- Orchestration system?
    - We could in theory but nobody has done it.
